#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );



static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

/*  Р°Р»Р»РѕС†РёСЂРѕРІР°С‚СЊ СЂРµРіРёРѕРЅ РїР°РјСЏС‚Рё Рё РёРЅРёС†РёР°Р»РёР·РёСЂРѕРІР°С‚СЊ РµРіРѕ Р±Р»РѕРєРѕРј */
static struct region alloc_region  ( void const * addr, size_t query ) {
  size_t size = region_actual_size(size_from_capacity((block_capacity){query}).bytes);
  void *reg_address = map_pages(addr, size, MAP_FIXED_NOREPLACE);
	if (reg_address == NULL || reg_address == MAP_FAILED){
        reg_address = map_pages(addr, size, 0);
		if (reg_address == NULL || reg_address == MAP_FAILED){
			return REGION_INVALID;
		}
	}
  
	block_init(reg_address, (block_size){ size }, NULL);
  return  (struct region){.addr = reg_address, .size = size, .extends = (addr== reg_address)};
}

static void* block_after( struct block_header const* block )         ;

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Р Р°Р·РґРµР»РµРЅРёРµ Р±Р»РѕРєРѕРІ (РµСЃР»Рё РЅР°Р№РґРµРЅРЅС‹Р№ СЃРІРѕР±РѕРґРЅС‹Р№ Р±Р»РѕРє СЃР»РёС€РєРѕРј Р±РѕР»СЊС€РѕР№ )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
  if (block == NULL || !block_splittable(block, query)) return false;
    block_size size = (block_size){block->capacity.bytes - query};
    void* address = block->contents + query; 
    block_init(address, size, block->next);
    block->next = address;
    block->capacity.bytes=query;
    return true;
 
}


/*  --- РЎР»РёСЏРЅРёРµ СЃРѕСЃРµРґРЅРёС… СЃРІРѕР±РѕРґРЅС‹С… Р±Р»РѕРєРѕРІ --- */

static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
  struct block_header *next_comp = block->next;
    if (next_comp == NULL || !mergeable(block, next_comp)) return false;
		block->capacity.bytes += size_from_capacity(next_comp->capacity).bytes;
		block->next = next_comp->next;
		return true;

}


/*  --- ... ecР»Рё СЂР°Р·РјРµСЂР° РєСѓС‡Рё С…РІР°С‚Р°РµС‚ --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};


static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz )    {
  
    if (block == NULL) {
        return (struct block_search_result) { BSR_CORRUPTED, NULL };
    }

  struct block_header *curr = block, *finish = block;
	while(curr){
    while(try_merge_with_next(curr));
    if(curr->is_free && block_is_big_enough(sz, curr)) return (struct block_search_result){BSR_FOUND_GOOD_BLOCK, curr};
    finish = curr;
    curr = curr->next;
  }
  return (struct block_search_result){BSR_REACHED_END_NOT_FOUND, finish};

}

/*  РџРѕРїСЂРѕР±РѕРІР°С‚СЊ РІС‹РґРµР»РёС‚СЊ РїР°РјСЏС‚СЊ РІ РєСѓС‡Рµ РЅР°С‡РёРЅР°СЏ СЃ Р±Р»РѕРєР° `block` РЅРµ РїС‹С‚Р°СЏСЃСЊ СЂР°СЃС€РёСЂРёС‚СЊ РєСѓС‡Сѓ
 РњРѕР¶РЅРѕ РїРµСЂРµРёСЃРїРѕР»СЊР·РѕРІР°С‚СЊ РєР°Рє С‚РѕР»СЊРєРѕ РєСѓС‡Сѓ СЂР°СЃС€РёСЂРёР»Рё. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
  
  struct block_search_result result = find_good_or_last(block, query);
	if(result.type == BSR_FOUND_GOOD_BLOCK) {
    split_if_too_big(result.block, query);
    result.block->is_free = false;
  }
	return result;
}




static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
    
    if (last == NULL) {
        return NULL;
    }

  struct region reg = alloc_region(block_after(last), query + offsetof(struct block_header, contents));
  
  if (region_is_invalid(&reg)) {
      return NULL;
  }

  last->next = reg.addr;

  if (try_merge_with_next(last)) {
      return last;
  }

  return last->next;
}

/*  Р РµР°Р»РёР·СѓРµС‚ РѕСЃРЅРѕРІРЅСѓСЋ Р»РѕРіРёРєСѓ malloc Рё РІРѕР·РІСЂР°С‰Р°РµС‚ Р·Р°РіРѕР»РѕРІРѕРє РІС‹РґРµР»РµРЅРЅРѕРіРѕ Р±Р»РѕРєР° */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
  size_t sz = size_max(query, BLOCK_MIN_CAPACITY);
  struct block_search_result result = try_memalloc_existing(sz, heap_start);

  if(result.type == BSR_REACHED_END_NOT_FOUND){
      result.block = grow_heap(result.block,sz);

      if (result.block == NULL) {
          return NULL;
      }


    result = try_memalloc_existing(sz, result.block);
  }

  return result.type == BSR_FOUND_GOOD_BLOCK? result.block: NULL;
}


void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  while (try_merge_with_next(header));
}
